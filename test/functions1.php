<?php
function connect() {
	$host = 'localhost';
	$db_name = 'emp';
	$db_user = 'root';
	$db_password = '';
    return new PDO('mysql:host='.$host.';dbname='.$db_name, $db_user, $db_password, 
	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}
$pdo = connect();




 
$csv_file =  $_FILES['csv_file']['tmp_name'];
if (is_file($csv_file)) {
	$input = fopen($csv_file, 'a+');
	// if the csv file contain the table header leave this line
	$row = fgetcsv($input, 1024, ','); // here you got the header
	while ($row = fgetcsv($input, 1024, ',')) {
		// insert into the database
		$sql = 'INSERT INTO employee(name, age, city) VALUES(:name, :age, :city)';
		$query = $pdo->prepare($sql);
		$query->bindParam(':name', $row[0], PDO::PARAM_STR);
		$query->bindParam(':age', $row[1], PDO::PARAM_STR);
		$query->bindParam(':city', $row[2], PDO::PARAM_STR);
		$query->execute();
	}
}
 
// redirect to the index page
//header('location: list.php');
?>