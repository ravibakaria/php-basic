<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_company";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
	
	

 if(isset($_POST["Export"])){
		 
      header('Content-Type: text/csv; charset=utf-8');  
      header('Content-Disposition: attachment; filename=data.xlsx');  
      $output = fopen("php://output", "w");  
      fputcsv($output, array('Id', 'Name', 'phone_no', 'Address', 'Salary', 'Emp Id', 'Roll','DOB'));  
      $query = "SELECT Id, Name, phone_no, Address, Salary, EmpId, Roll,DOB FROM employee  ";  
      $result = mysqli_query($conn, $query);  
	  foreach($result as $value)
   
      {  
           fputcsv($output, $value);  
      }  
      fclose($output);  
 }  
?>