<html>
<head>
<head>
		
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
 
</html>
<?php include 'header.php';
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_company";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//$sql = "SELECT `Id`, `Name`, `phone_no`, `Address`, `Salary`, `Emp Id`, `Roll`,`DOB`, `created_at`, `updated_at` FROM `employee`";
//$result = $conn->query($sql);

$where = " WHERE 1=1 "; // where clause will always run

if(isset($_REQUEST['name']) && trim($_REQUEST['name']) != '') { // this will run only when name is set in form
	
	$where = $where . " AND name LIKE '%" . trim($_REQUEST['name']) . "%'"; // append to existing sql

}

$sql = "SELECT * FROM `employee` $where";
$result = $conn->query($sql);

?>
<form class="form-horizontal" action="functions1.php" method="post" name="upload_excel" enctype="multipart/form-data">
	<fieldset>

		<!-- Form Name -->
		<legend>Form Name</legend>

		<!-- File Button -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="filebutton">Select File</label>
			<div class="col-md-4">
				<input type="file" name="csv_file" id="csv_file" class="input-large">
			</div>
		</div>

		<!-- Button -->
		<div class="form-group">
			<label class="col-md-4 control-label" for="singlebutton">Import data</label>
			<div class="col-md-4">
				<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
			</div>
		</div>

	</fieldset>
</form>

<?php


if ($result->num_rows > 0) {
    // output data of each row
	?>
	<div class="container">
    <table class="table table-striped">
	<tr>
	  <th>Id</th>
	  <th>Name</th>
	  <th>phone_no</th>
	  <th>Address</th>
	  <th>Salary</th>
	  <th>Emp Id</th>
	  <th>Roll</th>
	  <th>DOB</th>
	  <th>Action</th>
	</tr>

	
	<?php 
		if(isset($result)){
			foreach($result as $record){
	?>
		<tr>
			<td> <?php echo $record['Id']; ?></td>
			<td> <?php echo $record['Name']; ?></td>
			<td> <?php echo $record['phone_no']; ?></td>
			<td> <?php echo $record['Address']; ?></td>
			<td> <?php echo $record['Salary']; ?></td>
			<td> <?php echo $record['EmpId']; ?></td>
			<td> <?php echo $record['Roll']; ?></td>
			<td> <?php echo $record['DOB']; ?></td>
			<td> <a href="show.php?id=<?php echo $record['Id']; ?>">Edit </a>&nbsp; 
			 <a href="delete.php?id=<?php echo $record['Id']; ?>"/>Delete</td>
		</tr>
		
	<?php
		}
	}
}
	//header("Location:show.php");
	?>
	<tr>
		<td>
            <form class="form-horizontal" action="functions.php" method="post" name="upload_excel"   
                      enctype="multipart/form-data">
                  <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <input type="submit" name="Export" class="btn btn-success" value="export to excel"/>
                            </div>
                   </div>                    
            </form>           
		</td>
	</tr>