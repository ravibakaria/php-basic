<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "car_sell";

// Create connection  
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {  
    die("Connection failed: " . $conn->connect_error);
} 
 if (isset($_POST['upload'])) {
  	// Get image name
	$image = $_FILES['image']['name'];
  	$target = "images/".basename($image);

  	$sql = "INSERT INTO image (image) VALUES ('$image')";
  	if ($conn->query($sql) === TRUE) {
    echo "NEW Record Add Successfully";
	} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
	}
	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
		$msg = "Image uploaded successfully";
	}else{
		$msg = "Failed to upload image";
	}
  }
  $sql =  "SELECT * FROM image";
  $result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>
<head>
<title>Image Upload</title>
<style type="text/css">
   #content{
   	width: 50%;
   	margin: 20px auto;
   	border: 1px solid #cbcbcb;
   }
   form{
   	width: 50%;
   	margin: 20px auto;
   }
   form div{
   	margin-top: 5px;
   }
   #img_div{
   	width: 80%;
   	padding: 5px;
   	margin: 15px auto;
   	border: 1px solid #cbcbcb;
   }
   #img_div:after{
   	content: "";
   	display: block;
   	clear: both;
   }
   img{
   	float: left;
   	margin: 5px;
   	width: 300px;
   	height: 140px;
   }
</style>
</head>
<body>
<div id="content">
  <?php
  if ($result->num_rows > 0) {
    if(isset($result)){
			foreach($result as $record){
      echo "<div id='img_div'>";
      	echo "<img src='images/".$record['image']."' >";
      	
      echo "</div>";
    }
  }
  }
  ?>
  <form method="POST" action="" enctype="multipart/form-data">
  	<input type="hidden" name="size" value="1000000">
  	<div>
  	  <input type="file" name="image">
  	</div>
  	
  	<div>
  		<button type="submit" name="upload">POST</button>
  	</div>
  </form>
</div>
</body>
</html>