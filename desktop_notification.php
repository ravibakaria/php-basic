<html>
	<body>
		<h1>Hello World</h1>
	</body>
	<script>
		site_url = "Hello World";
		// request permission on page load
		document.addEventListener('DOMContentLoaded', function () {
		  if (!Notification) {
			alert('Desktop notifications not available in your browser. Try Firefox or Chromium.'); 
			return;
		  }

		  if (Notification.permission !== "granted")
			Notification.requestPermission();
		});

		function notifyMe() {
		  if (typeof Notification !== 'undefined' && Notification.permission !== "granted")
			Notification.requestPermission();
		  else {
			var notification = new Notification('Intense Scan | Disco', {
			  icon: site_url,
			  body: "Scanning Completed, Click to see the result!",
			});

			notification.onclick = function () {
			  window.focus();//window.open("http://stackoverflow.com/a/13328397/1269037");      
			};
		  }
		}
		notifyMe();
	</script>
</html>
