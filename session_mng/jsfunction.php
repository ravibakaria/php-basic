<script>
x = {
	'fname':'Ravi',
	'age': 26,
	'city':'mumbai',
	'show':function(){
		console.log("Name = " + this.fname);
		console.log("Age = " + this.age);
		console.log("City = " + this.city);
	}
}
x.show();
function Circle(r){
	this.radius = r;
	//console.log(r);
	Circle.prototype.area = function(){
		console.log("Area of Circle is = "+3.14 * this.radius * this.radius);
	};
	this.circumference = function(){
		console.log("Circumference of Circle  = "+2 * 3.14 * this.radius);
	}
}
obj = new Circle(5);
//console.log(obj);
console.log(obj.radius);
obj.area();
c2 = new Circle(9);
c2.area();
c2.circumference();
console.log(obj);
console.log(c2);
</script>