<?php

echo password_hash("passwords", PASSWORD_DEFAULT);
echo "</br>";

echo "</br>";
$firstName="";
if (!empty($firstName)) $name = $firstName;
else $name = "Guest";
echo $name;
echo "</br>";
$bytes = random_bytes(5);
var_dump(bin2hex($bytes));
echo "</br>";
random_int(1,20);
var_dump(random_int(1,20));
echo "</br>";
echo "\u{1F60D}";
echo "</br>";
function getTotal(float $a, float $b) {
    return $a + $b;
}
getTotal(2, "1 week"); 
?>

<?php 
// compares strings lexically 
var_dump('PHP' <=> 'Node'); // int(1) 
// compares numbers by size 
var_dump(123456 <=> 456); // int(-1) 
// compares corresponding array elements with one-another 
var_dump(['a', 'b'] <=> ['a', 'b']); // int(0)
?>
<?php
echo "</br>";
// Pre PHP 7 code 
$route = isset($_GET['route']) ? $_GET['route'] : 'index'; 
// PHP 7+ code 
$route = $_GET['route'] ?? 'index'; 
?>

<?php
echo "</br>"; 
// Coercive mode 
function sumOfInts(int ...$ints) 
{ 
    return array_sum($ints); 
} 
var_dump(sumOfInts(2, '3', 4.1)); // int(9)
?>

<?php 
echo "</br>"; 
		function arraysSum(array ...$arrays): array 
		{ 
			return array_map(function(array $array): int {
				return array_sum($array); 
			}, $arrays); 
		} 
		print_r(arraysSum([1,2,3], [4,5,6], [7,8,9])); 
		/* OutputArray(    [0] => 6    [1] => 15    [2] => 24)*/
		?>
		
		<?php
		echo "</br>"; 
var_dump(intdiv(10, 3)); // int(3)
?>

<?php
echo "</br>"; 
var_dump(substr('a', 1));// Pre PHP 7 resultbool(false)// PHP 7+ resultstring(0) ""
echo "</br>"; 
echo "\u{aa}"; // ª
echo "\u{0000aa}"; // ª (same as before but with optional leading 0's)
echo "\u{9999}"; 

?>
