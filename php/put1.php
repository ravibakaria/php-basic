<?php
	class Student {
		public $name;
		public $age;
		public $city;
		
		
		function __construct($n,$a,$c){
			$this->name=$n;
			$this->age=$a;
			$this->city=$c;
		}
		function __toString(){
			return "Name:".$this->name."<br>".
					"Age:".$this->age. "<br>".
					"City:".$this->city;
					
		}
	}
	
$std1 = new Student($_POST['name'],$_POST['age'],$_POST['city']);

$section = file_get_contents('append.txt');

if($section == "") {
	
	$records = array($std1);
	
} else {
	$records = json_decode($section, true);
	array_push($records, (array)$std1);

}

file_put_contents("append.txt", json_encode($records));

header("Location: put.php");


 ?>	
