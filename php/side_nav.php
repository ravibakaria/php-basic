<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
    section { border:1px solid red; padding:10px; overflow:hidden; }
    section > section { float:left; }
    .indent-1 { padding-left:120px; }
</style>

<section class="indent-1">
    <!-- Section 1 --> 
    <section>
        <div>Some content 1</div>
        <div>Some more 1</div>
    </section>

    <!-- Section 2 -->
    <section>
        <div>Some content 2</div>
        <div>Some more 2</div>
    </section>
</section>  