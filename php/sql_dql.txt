SQL-DQL Test
---------------------------------------------------------------
1. select employees in descending order - salary
Ans - SELECT * FROM employee ORDER BY Salary DESC;

2. select all employees from Mumbai
Ans - SELECT * FROM employee WHERE Address LIKE '%mumbai%';

3. select all employees having salary more than average salary
Ans - SELECT * FROM employee WHERE salary=(SELECT MAX(salary) as Salary FROM employee);

4. select sum of salary from table
Ans - SELECT SUM(Salary) AS Salary FROM employee;

5. select all unique address
Ans - SELECT DISTINCT Address FROM employee;

6. select details, salary from table
(details should be concatenation of name and address)
Ans - SELECT CONCAT(Name, Address) AS details, Salary FROM employee;

7. select names of employees having max salary
Ans - SELECT * FROM employee WHERE salary = (SELECT MAX(Salary) FROM employee);

8. select employees having 2nd max salary
Ans - SELECT * FROM employee WHERE Salary =(SELECT DISTINCT Salary FROM employee ORDER BY Salary DESC LIMIT 1,1)

9. count emplyees by address, order by employee count
e.g.
---------------------------
city	|	emplyee count |
---------------------------
Mumbai	|		 4		  |
Nagpur	| 		 3		  |
Pune	| 		 2		  |
---------------------------

10. count employees from mumbai only
Ans - SELECT COUNT(Address) FROM `employee` WHERE Address = 'mumbai';

11. select all employees whose names starts or ends with vowels

12. find employee having max salary in particular city
e.g. 
------------------------------------
address 	|	name 	|	salary	|	
------------------------------------|
noida 		|	preeti 	|	87000	|
ahmedabad 	|	anjali 	|	60000	|
bangalore 	|	madhu 	|	60000	|
baroda 		|	varun 	|	56000	|
------------------------------------